<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  
  <xsl:template match="/">
    <html>
      <body>
        <h2>PlayerData</h2>
        <table border="1" bgcolor="blue" font="Calibri">
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Position</th>
            <th>Team</th>
            <th>Nationality</th>
            <th>Age</th>
            <th>TotalGoals</th>
            <th>Salary</th>
          </tr>
          <xsl:for-each select="SoccerPlayer/Player">

            <tr>
              
              <td>
                                <xsl:value-of select="ID"/>
                            </td>
              <td>
                <xsl:value-of select="Name"/>
              </td>
              <td>
                <xsl:value-of select="Position"/>
              </td>
              <td>
                <xsl:value-of select="Team"/>
              </td>
              <td>
                <xsl:value-of select="Nationality"/>
              </td>
              <td>
                <xsl:value-of select="Age"/> 
              </td>
              <td>
                <xsl:value-of select="TotalGoals"/>
              </td>
              <td>
                <xsl:value-of select="Salary"/>
              </td>
          </tr>
          </xsl:for-each>
        </table>
      

      </body>
    </html>

  </xsl:template>
</xsl:stylesheet>