<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  
  <xsl:template match="/">
    <html>
      <body>
        <h2>ClubsData</h2>
        <table border="5" bgcolor="red" font="Times">
          <tr>
            <th>Name</th>
            <th>Founded</th>
            <th>League</th>
            <th>LeagueWins</th>
            <th>Standing</th>
           
          </tr>
          <xsl:for-each select="ClubData/Club">

            <tr>
              
              <td>
                <xsl:value-of select="Name"/>
              </td>
              <td>
                <xsl:value-of select="Founded"/>
              </td>
              <td>
                <xsl:value-of select="League"/>
              </td>
              <td>
                <xsl:value-of select="LeagueWins"/>
              </td>
              <td>
                <xsl:value-of select="CurrentLeagueStanding"/>
              </td>
              
          </tr>
          </xsl:for-each>
        </table>
       
      </body>
    </html>

  </xsl:template>
</xsl:stylesheet>