xquery version "3.0";
declare namespace output ="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "html5";
declare option output:media-type "text/html";

let $player:=doc("Player.xml")/SoccerPlayer
let $items:=$player/Player[Nationality="England"]
for $m in $player
return(
    <html>
        <body>
            <h1>Query 1</h1>
            <table border="1">
            <tr>
            <th>Country</th>
            <th>Total Goals</th>
            </tr>
        {
for $x in distinct-values( $player/Player/Nationality)
let $country:=$player/Player[Nationality=$x]
order by $x descending
return(
<tr> 
    <td>{$x}</td>
    <td>{avg($country/TotalGoals)}</td>
    </tr>
        
)
}
</table>
</body>
</html>
)


