xquery version "3.0";
declare namespace output ="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "html5";
declare option output:media-type "text/html";
let $players:=doc("Player.xml")/SoccerPlayer
for $m in $players
return(
    <html>
        <body>
            <h1>Query 2 </h1>
            <table border ="1">
                <tr>
                    <th> Name </th>
                    <th> Position </th>
                    <th> Salary </th>
                    </tr>
            {
for $x in ($players/Player)
where $x/Age<31 and not (contains($x/Positions/position,"Forward"))
order by $x/salary descending
return
    <tr><td>{data($x/Name)}</td>
<td>{data($x/Positions/position)}</td>
<td>{data($x/Salary)}</td> </tr>
}
</table>
</body>
</html>
)