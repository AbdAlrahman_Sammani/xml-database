
xquery version "3.0";
declare namespace output ="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "html5";
declare option output:media-type "text/html";

let $team:=doc("Team.xml")/ClubData/Club
let $d:=doc("Player.xml")/SoccerPlayer
let $player:=doc("Player.xml")/SoccerPlayer/Player
for $m in $d
return(
    <html>
        <body>
            <h1>Query 3 </h1>
            <table border="1">
            <tr>
                <th>League</th>
                <th>Avg Salary </th>
                </tr>
            {
for $legue in distinct-values($team/League)
let $lTeam:=$team[League=$legue]
let $lplay:=$player[Team=$lTeam/Name]
let $PLF:=$lplay[contains(Positions/position,"Forward")]
return <tr> 
<td>{$legue}</td>
<td>{avg($PLF/Salary)}</td>
</tr>

}
</table>
</body>
</html>
)