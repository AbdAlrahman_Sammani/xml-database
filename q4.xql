xquery version "3.0";
declare namespace output ="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "html5";
declare option output:media-type "text/html";

let $data:=doc("Team.xml")
let $team:=doc("Team.xml")/ClubData/Club
for $m in $data
return(
    <html>
        <body>
            <h1>Query 4 </h1>
            <table border ="1">
            <tr>
                <th>Team</th>
                <th>Standing</th>
                </tr>
            {
for $x in distinct-values($team/League)
let $l:=$x
let $lTeam:=$team[League=$l]


return <tr>
{
for $val in ($lTeam)
where $val/CurrentLeagueStanding=min($lTeam/CurrentLeagueStanding)
return <td>{data($val/Name)}</td>
}
<td>{min($lTeam/CurrentLeagueStanding)}</td>
</tr>
}
</table>
</body>
</html>
)